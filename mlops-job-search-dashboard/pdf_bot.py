#load libraries for the LLM widget
import os
import streamlit as st
from langchain.chains import RetrievalQA
from PyPDF2 import PdfReader
from langchain.callbacks.base import BaseCallbackHandler
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.vectorstores import Neo4jVector
from streamlit.logger import get_logger
from chains import (
    load_embedding_model,
    load_llm,
)

# load api key lib
from dotenv import load_dotenv

#load libraries for the other dashboard widgets (wordcloud of hacker news, top 10 google results for mlops job search, headlines from the Times newspaper)
import matplotlib.pyplot as plt
from wordcloud import WordCloud
import psycopg2
import pandas as pd
import datetime
from bs4 import BeautifulSoup
import requests

import altair as alt
import plotly.express as px


load_dotenv(".env")


url = os.getenv("NEO4J_URI")
username = os.getenv("NEO4J_USERNAME")
password = os.getenv("NEO4J_PASSWORD")
ollama_base_url = os.getenv("OLLAMA_BASE_URL")
embedding_model_name = os.getenv("EMBEDDING_MODEL")
llm_name = os.getenv("LLM")
# Remapping for Langchain Neo4j integration
os.environ["NEO4J_URL"] = url

logger = get_logger(__name__)


embeddings, dimension = load_embedding_model(
    embedding_model_name, config={"ollama_base_url": ollama_base_url}, logger=logger
)


class StreamHandler(BaseCallbackHandler):
    def __init__(self, container, initial_text=""):
        self.container = container
        self.text = initial_text

    def on_llm_new_token(self, token: str, **kwargs) -> None:
        self.text += token
        self.container.markdown(self.text)


llm = load_llm(llm_name, logger=logger, config={"ollama_base_url": ollama_base_url})

def the_times_headlines():
    url = "https://www.thetimes.co.uk/business-money"
    res = requests.get(url)
    soup = BeautifulSoup(res.text, 'html.parser')
    sections = soup.find_all('div', class_="css-6fg9h")

    headlines = []

    for section in sections:
        span_headlines = section.find_all('span', class_="css-17x5lw")
        for headline in span_headlines:
            # Check if the headline is not already in the list before appending
            if headline.text.strip() not in headlines:
                if headline.text.strip() != 'Money':
                    headlines.append(headline.text.strip())

    df_the_times_headlines = pd.DataFrame({'HEADLINES': headlines})
    return df_the_times_headlines



def query_google_results_table():
    """
    Task to query the SQL table and retrieve its contents.
    """
    conn_params = 'postgres://postgres:mysecretpassword@some-postgres-compose:5432/postgres'

    try:
        with psycopg2.connect(conn_params) as conn:
            with conn.cursor() as cur:
                # today = datetime.date.today().strftime('%Y-%m-%d')
                # query = "SELECT * FROM my_dataframe WHERE insertion_date = %s;"
                # cur.execute(query,(today,))
                query = "SELECT * FROM my_dataframe;"
                cur.execute(query)
                rows = cur.fetchall()
                columns = [desc[0] for desc in cur.description]
                df = pd.DataFrame(rows, columns=columns)
                return df
    except Exception as e:
        print(f"Error querying table: {e}")
        return None

# Function to fetch text data from PostgreSQL database
def fetch_data_from_database(today, yesterday):
    try:
        conn = psycopg2.connect(
            dbname="postgres",
            user="postgres",
            password="mysecretpassword",
            host="some-postgres-compose",
            port="5432"
        )
        cursor = conn.cursor()
        query = "SELECT comments FROM questions WHERE insertion_date = %s OR insertion_date = %s;"
        cursor.execute(query, (today, yesterday,))
        data = cursor.fetchall()
        cursor.close()
        conn.close()
        return data
    except psycopg2.Error as e:
        st.error(f"Database error: {e}")
        return []

# Function to generate word cloud from fetched text data
def generate_wordcloud(data):
    text = ' '.join([row[0] for row in data])
    wordcloud = WordCloud(width=800, height=400, background_color='white').generate(text)
    plt.figure(figsize=(10, 5))
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis('off')
    plt.show()
    plt.savefig('wordcloud.png')  # Save the plot to a file
    return plt.gcf()


def main():
    #set color theme and dashboard layout
    st.set_page_config(
    page_title="MLOPs Engineer Dashboard",
    page_icon="pineapple",
    layout="wide",
    initial_sidebar_state="expanded")
    alt.themes.enable("dark")


    date_format = "%Y-%m-%d"
    today = datetime.date.today().strftime(date_format)
    datetime_obj = datetime.datetime.strptime(today, date_format)
    # Extracting just the date component
    today = datetime_obj.date()
    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    yesterday= yesterday.date()  # Convert datetime to date for comparison



    col_pdf = st.columns((1), gap='medium')
    col_times_newspaper = st.columns((1), gap='medium')
    col_word_cloud_hacker_news = st.columns((1), gap='medium')
    col_google_results_table = st.columns((1), gap='medium')
    col_google_results_bar_chart = st.columns((1), gap='medium')


    word_cloud_data = fetch_data_from_database(today, yesterday)


    all_google_results = query_google_results_table()
    all_google_results['insertion_date'] = pd.to_datetime(all_google_results['insertion_date']).dt.date
    top_10_google_search_results_df = all_google_results[all_google_results['insertion_date'] == today]


    with col_pdf[0]:
        col_pdf[0].header("📄Chat with your pdf file")

        # upload a your pdf file
        pdf = st.file_uploader("Upload your PDF", type="pdf")

        if pdf is not None:
            pdf_reader = PdfReader(pdf)

            text = ""
            for page in pdf_reader.pages:
                text += page.extract_text()

            # langchain_textspliter
            text_splitter = RecursiveCharacterTextSplitter(
                chunk_size=1000, chunk_overlap=200, length_function=len
            )

            chunks = text_splitter.split_text(text=text)

            # Store the chunks part in db (vector)
            vectorstore = Neo4jVector.from_texts(
                chunks,
                url=url,
                username=username,
                password=password,
                embedding=embeddings,
                index_name="pdf_bot",
                node_label="PdfBotChunk",
                pre_delete_collection=True,  # Delete existing PDF data
            )
            qa = RetrievalQA.from_chain_type(
                llm=llm, chain_type="stuff", retriever=vectorstore.as_retriever()
            )

            # Accept user questions/query
            query = st.text_input("Ask questions about your PDF file")

            if query:
                stream_handler = StreamHandler(st.empty())
                qa.run(query, callbacks=[stream_handler])


    with col_times_newspaper[0]:
        col_times_newspaper[0].header('THE TIMES: Business & Money')
        df_the_times_headlines = the_times_headlines()
        st.table(df_the_times_headlines)
    

    with col_word_cloud_hacker_news[0]:
        col_word_cloud_hacker_news[0].header('Hacker News: What are people talking about today')
        if not word_cloud_data:
            st.warning("No data fetched from the database.")
        else:
            # Generate and display word cloud
            word_cloud_plot = generate_wordcloud(word_cloud_data)
        st.pyplot(word_cloud_plot)


    with col_google_results_table[0]:
        displayed_df = top_10_google_search_results_df[['search_result_title', 'job_count','title_links']]
        col_google_results_table[0].header(f'Top 10 Google Results: glassdoor mlops engineer jobs')
        st.table(displayed_df)

    with col_google_results_bar_chart [0]:
        result_df = all_google_results.groupby('insertion_date').sum().reset_index()
        result_df.set_index('insertion_date', inplace=True)
        st.bar_chart(result_df['job_count'])




if __name__ == "__main__":
    main()
