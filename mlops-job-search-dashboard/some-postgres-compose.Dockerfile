# Use the official PostgreSQL image as a starting point
FROM postgres:latest

# Example: Install additional PostgreSQL extensions here
# RUN apt-get update && apt-get install -y postgresql-contrib

# Copy initialization scripts if you have any. For example:
# COPY ./init-scripts/ /docker-entrypoint-initdb.d/

# No CMD or ENTRYPOINT override is needed; we use what's provided by the base image
